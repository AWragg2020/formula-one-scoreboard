import os.path
from flask import Flask

# Neccessary additional imports
import json
import random
from flask import jsonify, make_response

SRC_DIR = os.path.abspath(os.path.dirname(__file__))
DATA_DIR = os.path.join(SRC_DIR, 'data')

app = Flask(__name__)

@app.route('/')
def home():
    return app.send_static_file('index.html')

@app.route('/api/standings.json')
def standings():
    # Opens teams.json and references it as json_file
    with open('data/drivers.json') as json_file:
        # Stores teams.json's info in 'data'
        data = json.load(json_file)
        # Selects a random element from the data (using the library 'random')
        elem = random.choice(data)
        # Increments by 1 the 'points' property of the randomly selected element
        elem['points'] += 1
        print elem
        # Re-dumps the info in data as a json in drivers.json
        with open('data/drivers.json', 'w') as outfile:
            json.dump(data, outfile)

        # Makes a call to the auxiliary function team_name in order to get the driver's team name with the driver's teamId
        # This also adds a new property, 'team_name', to the driver element in data, so it will be binded and shown in the view
        for i in data:
            i['team_name'] = team_name(i['team'])

        # Gives answer to the front in the form of a json which data has been formatted in 'data'
        # Code 200: OK, for cleanness :)
        return make_response(jsonify({'data':data}), 200)


@app.route('/api/team/<int:team_id>.json')
def team_details(team_id):
    with open('data/teams.json') as json_file:
        data = json.load(json_file)
        # Iterates throught data's elements and chooses only that which's teamId matches team_id 
        item = next((i for i in data if i['id'] == team_id), None)
        # If it is found, returns the team's details to the front as a json. Again, response code 200.
        # If it is not found, returns an error with message 'not found' and code 404 (not found)
        if item is not None:
            return make_response(jsonify(item), 200)
        else:
            return make_response(jsonify({'error':'not found'}), 404)

# Auxiliary function team_name, that retrieves the team's name (string) based on the inputed team_id
def team_name(team_id):
    with open('data/teams.json') as json_file:
        data = json.load(json_file)
        item = next((i for i in data if i['id'] == team_id), None)
        # Returns the item's team if it was found, returns empty string otherwise
        if item is not None:
            return item['team']
        else:
            return ''


if __name__ == '__main__':
    app.run(debug=True)
