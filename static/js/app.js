var app = angular.module('formulaOne', ["ngRoute"]);

// Configuration of routing:
// When in root: the app will load main.htm which controller is DriversCtrl (showing the scoreboard with the drivers)
// When a driver is selected, it will grab its team's id and go to team.htm, which controller is TeamCrtl
app.config(function($routeProvider) {
  $routeProvider
  .when("/", {
    templateUrl : "/static/main.htm",
    controller : "DriversCtrl"
  })
  .when("/team/:idTeam", {
    templateUrl : "/static/team.htm",
    controller : "TeamCtrl"
  })
});

//app.controller('MainCtrl', function($scope) {});

// Controller for scoreboard view
app.controller('DriversCtrl', function($scope, $http, $interval, $location, Drivers) {

    // Prompt so we can check it is loaded
    console.log('DriversCtrl');

    // Declaration of a variable that will make the call every second to the function that will
    // fetch the list with the drivers and teams (the scoreboard)
    var promise;

    // Call to the back that retrieves the data for the scoreboard - so it can display them in the view
    $scope.fn = function(){
        $scope.drivers = [];
        var url = "http://localhost:5000/api/standings.json";
        $http.get(url).then(
          function (data) {
            $scope.drivers = [];
            var arr = data.data['data'];
            for (let key in arr) {
              var count = arr[key].country;
              if (count == "uk") count = "gb"; // Special case for uk (so it shows the proper flag thanks to bower's dependency flag-icon-css)
              $scope.drivers.push({
                "driver":arr[key].driver,
                "points":arr[key].points,
                "country":count,
                "team_name":arr[key].team_name,
                "team":arr[key].team
              });
            }
          },
          // In case of response error
          function (error) {
            console.log(error);
          }
        )
     }


    // Start function that makes the call to the back every second
    $scope.start = function() {
      promise = $interval(function () {
          $scope.fn()
      }, 1000)
    };

    // Function that stops the calls to the back every second
    $scope.stop = function() {
      $interval.cancel(promise);
    };
    $scope.start();

    // On destroy, call stop()
    $scope.$on('$destroy', function() {
      $scope.stop();
    });

    // Function to go to the view of the selected team
    $scope.goTeam = function(teamId){

      // Assigns the $scope's variable drivers to the Drivers object
      Drivers.set($scope.drivers);
      $location.path("/team/" + teamId);

    };
});

// Controller for the team view, called TeamCtrl
app.controller('TeamCtrl', function($scope, $http, $routeParams, Drivers) {

    // Collects the idTeam from the url parameters and stores it in $scope.idTeam
    $scope.idTeam = $routeParams.idTeam;
    console.log('TeamCtrl: ' + $scope.idTeam);

    // Gets the drivers and stores in $scope.drivers in order to visualize only the ones with the same teamId
    $scope.drivers = Drivers.get();
    console.log('Drivers: ' + $scope.drivers);

    // Initialization of variables for the team's name and the car's name
    $scope.team_name = '';
    $scope.car = '';

    // Call to the back so it retrieves the drivers associated with the selected team
    var url = "http://localhost:5000/api/team/"+$scope.idTeam+".json";
    $http.get(url).then(
      function (data) {
        console.log(data);

        $scope.team_name = data.data['team'];
        $scope.car = data.data['car'];

      },
      // In case of call error
      function (error) {
        console.log(error);
      }
    )


});

// Factory in order to define the Driver object with functions set, get and add
app.factory('Drivers', function () {
    var driv = [];

    return {
        // To set a single data element to driv
        set: function (data) {
            driv = data;
        },
        // To retrieve the driv's value
        get: function () {
            return driv;
        },
        // To add a data element to driv
        add: function (item) {
            driv.push(item);
        }
     }
});
